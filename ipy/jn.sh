#!/usr/bin/env bash

. .venv/bin/activate
jupyter-notebook --ip='127.0.0.1'
deactivate
