
# Multi browser Testing / Cross-browser Testing

In today's world, we can not just rely by testing the web application on one browser. QA needs to test the same test-cases on multiple browsers across multiple versions as well. 

Selenium has been designed with this issue in mind and almost all code of test-cases can be reused for different browsers. Another thing to note is that once the web driver is created for particular browser, rest of all the code is browser independent.

Depending on the nature of testing multiple ways can be employed to execute the cross browser testing. 

## UI validation

One of the most common UI validation method is to let the functional testing take the screenshots on important UI validation points while executing the testcases against multiple browser/version combinations and at the end using simple python script compare the differences between the images. 

## Functional Testing

Create your testcases to test the functionality of the application independent of browsers. Browser selection can be done at the runtime using some configuration or command line as shown in the below code.

```python
import unittest
from unittest import suite
import configparser

from framework.utils import line
from qumu_tests import login_tests, home_tests


class TestLoaderWithKwargs(unittest.TestLoader):
    """TestLoader.

    A test loader which allows to parse keyword arguments to the
    test case class."""

    def loadTestsFromTestCase(self, testCaseClass, **kwargs):
        """Return a suite of all tests cases contained in testCaseClass."""
        if issubclass(testCaseClass, suite.TestSuite):
            raise TypeError("Test cases should not be derived from "
                            "TestSuite. Maybe you meant to derive from"
                            " TestCase?")
        testCaseNames = self.getTestCaseNames(testCaseClass)
        if not testCaseNames and hasattr(testCaseClass, 'runTest'):
            testCaseNames = ['runTest']

        # Modification here: parse keyword arguments to testCaseClass.
        test_cases = []
        for test_case_name in testCaseNames:
            test_cases.append(testCaseClass(test_case_name, **kwargs))
        loaded_suite = self.suiteClass(test_cases)

        return loaded_suite


def main():
    """Main engine for the automation."""
    config = configparser.ConfigParser()
    config.read('runbook.ini')
    modulesToRun = config['run']['modules']
    browsers = config['run']['browsers']
    moduleList = modulesToRun.split(",")
    browserList = browsers.split(",")

    line()
    print("Runbook details:")
    print("Modules to test: {0}".format(moduleList))
    print("Browsers: {0}".format(browserList))
    if len(moduleList) > 0 and len(browserList) > 0:
        line()
        suites = unittest.TestSuite()
        loader = TestLoaderWithKwargs()
        for browser in browserList:
            for module in moduleList:
                print("Adding module: " + module)
                suites.addTests(loader.loadTestsFromTestCase(eval(module),
                                                             browser=browser))
        unittest.TextTestRunner(verbosity=2).run(suites)
    else:
        print("Missing either modules or browsers in runbook")

if __name__ == '__main__':
    main()

```

```ini
; runbook.ini
; This file contains the runbook for the tests to be performed.
; Multiple values can be provided by using `,` as seperators
; example
; modules = LoginBasePage,HomePage
; browsers = chrome, firefox
[run]
modules=login_tests.LoginTests
browsers=chrome
language=en

[suiteConfig]
testFolder = qumu_tests
```

## References

Sample framework: https://bitbucket.org/Mayank_Johri/pyautomationframework. It supports testing of testcases against multiple browsers.
