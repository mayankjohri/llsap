Summary
Properties of the Alert class
Methods of the Alert class
Automating browser navigation
Summary
Cleaning up the code
Running the test
Adding another test
Class-level setUp() and tearDown() methods
Assertions
Test suites
Generating the HTML test report
Summary
Table of Contents
Learning Selenium Testing Tools with Python
Credits
About the Author
About the Reviewers
www.PacktPub.com
Support files, eBooks, discount offers, and more
Why subscribe?
Free access for Packt account holders
Preface
What this book covers
What you need for this book
Who this book is for
Conventions
Reader feedback
Customer support
Downloading the example code
Errata
Piracy
Questions
1. Getting Started with Selenium WebDriver and Python
Preparing your machine
Installing Python
Installing the Selenium package
Browsing the Selenium WebDriver Python documentation
Selecting an IDE
PyCharm
The PyDev Eclipse plugin
PyScripter
Setting up PyCharm
Taking your first steps with Selenium and Python
Cross-browser support
Setting up Internet Explorer
Setting up Google Chrome
Summary
2. Writing Tests Using unittest
The unittest library
The TestCase class
The setUp() method
Writing tests

3. Finding Elements
Using developer tools to find locators
Inspecting pages and elements with Firefox using the Firebug add-in
Inspecting pages and elements with Google Chrome
Inspecting pages and elements with Internet Explorer
Finding elements with Selenium WebDriver
Using the find methods
Finding elements using the ID attribute
Finding elements using the name attribute
Finding elements using the class name
Finding elements using the tag name
Finding elements using XPath
Finding elements using CSS selectors
Finding links
Finding links with partial text
Putting all the tests together using find methods
Summary
4. Using the Selenium Python API for Element Interaction
Elements of HTML forms
Understanding the WebDriver class
Properties of the WebDriver class
Methods of the WebDriver class
Understanding the WebElement class
Properties of the WebElement class
Methods of the WebElement class
Working with forms, textboxes, checkboxes, and radio buttons
Checking whether the element is displayed and enabled
Finding the element attribute value
Using the is_selected() method
Using the clear() and send_keys() methods
Working with dropdowns and lists
Understanding the Select class
Properties of the Select class
Methods of the Select class
Working with alerts and pop-up windows
Understanding the Alert class
5. Synchronizing Tests
Using implicit wait
Using explicit wait
The expected condition class
Waiting for an element to be enabled
Waiting for alerts
Implementing custom wait conditions
Summary
6. Cross-browser Testing
The Selenium standalone server
Downloading the Selenium standalone server
Launching the Selenium standalone server
Running a test on the Selenium standalone server
Adding support for Internet Explorer
Adding support for Chrome
Selenium Grid
Launching Selenium server as a hub
Adding nodes
Adding an IE node
Adding a Firefox node
Adding a Chrome node
Mac OS X with Safari
Running tests in Grid
Running tests in a cloud
Using Sauce Labs
Summary
7. Testing on Mobile
Introducing Appium
Prerequisites for Appium
Setting up Xcode for iOS
Setting up Android SDK
Setting up the Appium Python client package
Installing Appium
Appium Inspector
Testing on iOS
Writing a test for iOS
Testing on Android
Writing a test for Android
8. Page Objects and Data-driven Testing
Data-driven testing
Using ddt for data-driven tests
Installing ddt
Creating a simple data-driven test with ddt in unittest
Using external data sources for data-driven tests
Reading values from CSV
Reading values from Excel
The page objects pattern
Organizing tests
The BasePage object
Implementing page objects
Creating a test with page objects
Summary
9. Advanced Techniques of Selenium WebDriver
Methods for performing keyboard and mouse actions
Keyboard actions
The mouse movement
The double_click method
The drag_and_drop method
Executing JavaScript
Capturing screenshots of failures
Recording a video of the test run
Handling pop-up windows
Managing cookies
Summary
10. Integration with Other Tools and Frameworks
Behavior-Driven Development
Installing Behave
Writing the first feature in Behave
Implementing a step definition file for the feature
Creating environment configurations
Running features
Using a scenario outline
CI with Jenkins
Preparing for Jenkins
Setting up Jenkins
Summary
Inde