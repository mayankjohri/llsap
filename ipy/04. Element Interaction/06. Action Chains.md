
##  Action Chains

ActionChains provides a way to automate low level interactions such as mouse movements, mouse button actions, key press, and context menu interactions. It is mostly useful for doing more complex actions such as hover over and drag and drop.

**Generate user actions.**

When you call methods for actions on the ActionChains object, the actions are stored in a queue in the ActionChains object. When you call perform(), the events are fired in the order they are queued up.

ActionChains can be used in a chain pattern:

```Python
ActionChains(driver).move_to_element(menu).click(menu).click(hidden_submenu).perform()
```

Or actions can be queued up one by one to make it more readable and then use `perform` keyword to execute them in sequence, as shown below

```python
actions = ActionChains(driver)
actions.move_to_element(menu)
actions.click(menu)
actions.click(hidden_submenu)
actions.perform()
```

Following actions can be performed by `ActionChains`

- click(on_element=None)
- click_and_hold(on_element=None)
- context_click(on_element=None)
- double_click(on_element=None)
- drag_and_drop(source, target)
- drag_and_drop_by_offset(source, xoffset, yoffset)
- key_down(value, element=None)
- key_up(value, element=None)
- move_by_offset(xoffset, yoffset)
- move_to_element(to_element)
- move_to_element_with_offset(to_element, xoffset, yoffset)
- pause(seconds)
- perform
- release(on_element=None)
- reset_actions()
- send_keys(*keys_to_send)
- send_keys_to_element(element, *keys_to_send)

### click

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver import ActionChains


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/bootstrap_menu")

menu = driver.find_element_by_css_selector(".nav-item.dropdown")
hidden_submenu = driver.find_element_by_css_selector(" #link2")

actions = ActionChains(driver)
actions.move_to_element(menu)
actions.click(menu)
actions.click(hidden_submenu)
actions.perform()
```

### perform

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver import ActionChains


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/bootstrap_menu")

menu = driver.find_element_by_css_selector(".nav-item.dropdown")
hidden_submenu = driver.find_element_by_css_selector(" #link2")

actions = ActionChains(driver)
actions.move_to_element(menu)
actions.click(menu)
actions.click(hidden_submenu)
actions.perform()
```

### Executing Script

$$TODO$$

#### Execute Script

$$TODO$$

#### Execute Async Script

$$TODO$$
