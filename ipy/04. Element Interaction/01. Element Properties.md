
## Element Properties

Once we find the element, lets try to get the details of the elements.

- text
- size
- rect
- parent
- location
- tag_name
- is_enabled()
- is_selected()
- is_displayed()
- get_attribute(attr_name)
- get_property(prop_name)
- value_of_css_property(property_name)
- `driver.page_source`

### `element.text`

`element.text` `returns the text` within the element, excluding `html` formatting. Sample code is available as `view_text_find_by_class.py` 

```python
#!/usr/bin/env python
# coding=utf-8

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("https://www.reddit.com/")
ele = driver.find_element_by_class_name("trending-subreddits-content")
print(ele.text)
```

** Output : **
```
trending subredditsr/stevenuniverser/blurrypicturesofcatsr/JapaneseGameShowsr/ProperAnimalNamesr/IRLEasterEggs63 comments
```
![image\view_text.png](image\view_text.png)

The original element can be seen in the images above, note the spacing between the texts. 

### size

`element.size` returns the **size** of the element. Sample code is available as `view_size_find_by_class.py` 

```Python
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("https://www.reddit.com/")
ele = driver.find_element_by_class_name("trending-subreddits-content")
print(ele.size)
```

** Output : **
```
{'height': 17.5, 'width': 959.5}
```
![image\view_text.png](image\view_text.png)

The original element can be seen in the images above. 

### element.rect

`element.rect` returns the rect cordinates of the element. Sample code is available as `view_rect_find_by_class.py`

```Python
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("https://www.reddit.com/")
ele = driver.find_element_by_class_name("trending-subreddits-content")
print(ele.rect)

```

** Output : **
```
{'x': 84.5, 'y': 229.0, 'width': 959.5, 'height': 17.5}
```
![image\view_text.png](image\view_text.png)

The original element can be seen in the images above.

### element.parent

`element.parent` returns `Internal reference` to the `WebDriver` instance this element was found from. Sample code is available as `view_parent_find_by_class.py`

```Python
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("https://www.reddit.com/")
ele = driver.find_element_by_class_name("trending-subreddits-content")
print(ele.parent)
```

** Output : **
```
<selenium.webdriver.firefox.webdriver.WebDriver (session="b30a5156-d931-47f5-915b-1d0e204604ce")>
```

### element.location

`element.location` returns the location of the element in the renderable canvas. Sample code is available as `view_location_find_by_class.py`

```Python
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("https://www.reddit.com/")
ele = driver.find_element_by_class_name("trending-subreddits-content")
print(ele.location)

```

** Output : **
```
{'x': 84, 'y': 320}
```
![image\view_text.png](image\view_text.png)

The original element can be seen in the images above.

### element.tag_name

`element.tag_name` returns the rect cordinates of the element. Sample code is available as `view_tag_name_find_by_class.py`

```Python
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("https://www.reddit.com/")
ele = driver.find_element_by_class_name("trending-subreddits-content")
print(ele.tag_name)

```

** Output : **
```
div
```
![image\view_text.png](image\view_text.png)

The original element can be seen in the images above.

### element.is_enabled

`element.is_enabled` returns `True` if the element is enabled else return `False`. Sample code is available as `is_enabled.py`

```Python
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("http://localhost:5000/")
ele = driver.find_element_by_id("pineapple")
print(ele.is_enabled())
ele = driver.find_element_by_id("banana")
print(ele.is_enabled())
```

** Output : **
```
True
False
```

### element.is_displayed

`element.is_displayed` returns `True` if the element is displayed else return `False`. Sample code is available as `is_displayed.py`

```Python
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("http://localhost:5000/")
ele = driver.find_element_by_id("pineapple")
print(ele.is_displayed())
ele = driver.find_element_by_id("21st")
print(ele.is_displayed())

```

** Output : **
```
True
False
```

### element.is_selected

`element.is_selected` returns `True` if the element is selected else return `False`. Sample code is available as 'is_selected.py`

```Python
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("http://localhost:5000/")
ele = driver.find_element_by_id("pineapple")
print(ele.is_selected())
ele = driver.find_element_by_id("21st")
print(ele.is_selected())
```

** Output : **
```
True
False
```

### element.get_attribute

`element.get_attribute` returns the property or attribute of the element. 
This method will first try to return the value of a property with the given name. If a property with that name doesn’t exist, it returns the value of the attribute with the same name. If there’s no attribute with that name, None is returned.

Values which are considered truthy, that is equals “true” or “false”, are returned as booleans. All other non-None values are returned as strings. For attributes or properties which do not exist, None is returned.

Sample code is available as `get_attribute.py`

```Python
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("http://localhost:5000/")
ele = driver.find_element_by_id("pineapple")
print(ele.get_attribute("id"))
ele = driver.find_element_by_id("banana")
print(ele.get_attribute("text"))
```

** Output : **
```
pineapple
Banana

```

### element.get_property

`element.get_property` returns the value of given property of the element. Sample code is available as `get_property.py`

```Python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("http://localhost:5000/")
ele = driver.find_element_by_id("pineapple")
print(ele.get_property("disabled"))
ele = driver.find_element_by_id("banana")
print(ele.get_property("text"))
```

** Output : **
```
False
Banana
```

### What is the difference between `property` and `attribute`

@@@@ TODO @@@@

### element.value_of_css_property

`element.value_of_css_property` returns the value of css property of the element. Sample code is available as `value_of_css_property.py`

```Python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("http://localhost:5000/")
ele = driver.find_element_by_id("pineapple")
print(ele.value_of_css_property("background-color"))
ele = driver.find_element_by_id("banana")
print(ele.value_of_css_property("color"))
```

** Output : **
```
rgb(74, 144, 217)
rgb(139, 142, 143)
```

### `driver.page_source`

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


binary = FirefoxBinary('/opt/firefox/firefox')
driver = webdriver.Firefox(firefox_binary=binary)
driver.get("http://localhost:5000/")
print(driver.page_source())
```
