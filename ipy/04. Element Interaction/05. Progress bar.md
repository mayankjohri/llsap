
## Progress bar

### bootstrap progress bar

```Python
#!/usr/bin/env python
# coding=utf-8
# progress_bar.py
"""."""
from selenium import webdriver


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_progress")

# click radio button
progresses = driver.find_elements_by_class_name("progress")

for prog in progresses:
    print(prog.find_element_by_class_name("progress-completed").text)
    print(prog.value_of_css_property("width"))
```

### Html5 Progress bar

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://localhost:5000/form_html5_progress")

# click radio button
progress = driver.find_element_by_tag_name("progress")


print(progress.text)
print(progress.value_of_css_property("width"))
print(progress.get_attribute("value"))
```
