
## Waits

There are times in modern web application testing, where an `AJAX` call has been made and `webdriver` has to `wait` for the call to complete. Selenium has two types of `waits` available for these situations.

### Implicit Wait

Implicit waits provides a **default waiting time** (approx 30 seconds) between consecutive test steps for element(s) to be discovered. Thus step under execution will wait for max of implicit wait time while trying to find the element. If it finds the element within that time frame then it proceeds as soon as the object is available otherwise will fail after implicit wait time is over. 

```python
driver.implicitly_wait(TimeOut)
```

##### Example

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

driver.implicitly_wait(1)

try:
    not_found = driver.find_element_by_id("not_found")
except Exception as e:
    print(e)
```

### Explicit Waits

An `explicit wait` is a code which waits for a predifined condition to happen before continuing. 

The most basic example of it is `time.sleep()`, which waits for the said period of time and then proceeds. 

Selenium also provides some methods which allows to wait for certian condition to happen before proceeding. `WebDriverWait` in combination with `ExpectedCondition` is one such method.

### expected_conditions

`expected_conditions` has many functions which can be used to validate various conditions, we are going to cover most of them.

- title_is
- title_contains
- presence_of_element_located
- visibility_of_element_located
- visibility_of
- presence_of_all_elements_located
- text_to_be_present_in_element
- text_to_be_present_in_element_value
- frame_to_be_available_and_switch_to_it
- invisibility_of_element_located
- element_to_be_clickable
- staleness_of
- element_to_be_selected
- element_located_to_be_selected
- element_selection_state_to_be
- element_located_selection_state_to_be
- alert_is_present

#### `text_to_be_present_in_element`

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

try:
    element = WebDriverWait(driver, 15).until(
        ec.text_to_be_present_in_element((By.ID, "msg"), "Times out")
    )
finally:
    driver.quit()
```

#### `title_is`

`title_is` waits till the `page title` is same as provided or defined timeout.  

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

try:
    element = WebDriverWait(driver, 15).until(
        ec.title_is("!!! Done !!!")
    )
finally:
    driver.quit()
```

Below code waits for 5 seconds and fails with TimeoutException, and shows how custom error message can be shown before closing the automation. 

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException

options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

try:
    element = WebDriverWait(driver, 5).until(
        ec.title_is("!!! Done !!!")
    )
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
```

#### `title_contains`

```Python
#!/usr/bin/env python
# coding=utf-8
# wait_title_contains.py
"""."""
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException

options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

try:
    element = WebDriverWait(driver, 15).until(
        ec.title_contains("Done")
    )
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
```

#### presence_of_element_located

`presence_of_element_located` waits for the element to be found by webdriver.

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

try:
    element = WebDriverWait(driver, 15).until(
        ec.presence_of_element_located((By.ID, "swagatam"))
    )
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
```

#### visibility_of_element_located

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

try:
    element = WebDriverWait(driver, 15).until(
        ec.visibility_of_element_located((By.ID, "msg"))
    )
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
```

#### `presence_of_all_elements_located`

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

try:
    element = WebDriverWait(driver, 15).until(
        ec.presence_of_all_elements_located((By.CLASS_NAME, "num"))
    )
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
```

#### `text_to_be_present_in_element_value`

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

try:
    element = WebDriverWait(driver, 15).until(
        ec.text_to_be_present_in_element_value((By.ID, "timer"), "5")
    )
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
```

#### invisibility_of_element_located

It waits while the object is visible and proceeds once the object is not visible. It can be used to wait while `loaders` are present.

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

try:
    element = WebDriverWait(driver, 15).until(
        ec.invisibility_of_element_located((By.ID, "wait"))
    )
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
```

#### element_to_be_clickable

It waits for the event to be visible & enabled so that it can become clickable.

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/countdown")

try:
    element = WebDriverWait(driver, 15).until(
        ec.element_to_be_clickable((By.ID, "txt_no"))
    )
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
```

#### staleness_of

`staleness_of` checks for the `staleness` state of the element and acts accordingly. Now the next question is 
- **What is `staleness` state**: 
    `staleness` is the state of the DOM element existence. Once the DOM element is deleted, the `staleness` state changes to `True` as shown in the below example.
- **Where we can use it**: We can use it to wait for
    - Wait for custom dialogs to complete its execution and deleted by the Web Application
    - Wait for Components to be deleted by web application    

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/opt/google/chrome/chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/timeout")

try:
    ele = driver.find_element_by_id("ohm")
    WebDriverWait(driver, 15).until(
        ec.staleness_of(ele)
    )
except TimeoutException as e:
    print("Error: element was still present", e)
finally:
    driver.quit()
```

#### element_to_be_selected

In the below example, selenium driver waits till the `select option` with value `2` is not selected. 

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/opt/google/chrome/chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/")

try:
    css_val = "#exampleSelect2 > option[value='2']"
    ele = driver.find_element_by_css_selector(css_val)
    WebDriverWait(driver, 15).until(
        ec.element_to_be_selected(ele)
    )
except TimeoutException as e:
    print("Error: element was still present", e)
finally:
    driver.quit()
```

#### element_located_to_be_selected

`element_located_to_be_selected` is similar to `element_to_be_selected`, with one difference: in it we provide locator instead of element as in `element_to_be_selected`.

In `element_located_to_be_selected`, we need to provide a tuple with `(by, path)`.

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/opt/google/chrome/chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/")

try:
    css_val = "#exampleSelect2 > option[value='2']"
    WebDriverWait(driver, 15).until(
        ec.element_located_to_be_selected((By.CSS_SELECTOR, css_val))
    )
except TimeoutException as e:
    print("Error: element was still present", e)
finally:
    driver.quit()
```

- **When should we use it**:
    We can use it when the element is dynamic is nature, as shown in the below example. In it the element is created dynamically, thus element is not present at the start of wait and as a result we can't use `element_to_be_selected` as it needs an existing element.

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--enable-automation")
options.binary_location = "/opt/google/chrome/chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/")

try:
    css_val = "#exampleSelect2 > option[value='10']"
    WebDriverWait(driver, 15).until(
        ec.element_located_to_be_selected((By.CSS_SELECTOR, css_val))
    )
except TimeoutException as e:
    print("Error: element was still present", e)
finally:
    driver.quit()
```

#### element_selection_state_to_be

`element_selection_state_to_be` is used for checking if the given element is selected or not selected depending upon the second option. 
```python
driver.element_selection_state_to_be(<element>, selection_state)
```
- element is web_element object whose selection state we are checking
- selection_state is a Boolean (`True/False`), 

In the below code as the value of `selection_state` is `True`, it waits for the element `ele` to be selected.

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/opt/google/chrome/chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/")

try:
    css_val = "#exampleSelect2 > option[value='2']"
    ele = driver.find_element_by_css_selector(css_val)
    WebDriverWait(driver, 15).until(
        ec.element_selection_state_to_be(ele, True)
    )
except TimeoutException as e:
    print("Error: ", e)
finally:
    driver.quit()

```

#### element_located_selection_state_to_be

It is similar to `element_selection_state_to_be`, with one difference: in it we provide locator instead of element as in `element_selection_state_to_be`.

In `element_located_selection_state_to_be`, we need to provide a tuple with (by, path).

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/opt/google/chrome/chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/")

try:
    css_val = "#exampleSelect2 > option[value='2']"
    WebDriverWait(driver, 15).until(
        ec.element_located_selection_state_to_be((By.CSS_SELECTOR, css_val),
                                                 True)
    )
except TimeoutException as e:
    print("Error:", e)
finally:
    driver.quit()
```

#### alert_is_present

```python
#!/usr/bin/env python
# coding=utf-8
# wait_title_contains.py
"""."""
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/opt/google/chrome/chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/alert")

try:
    WebDriverWait(driver, 15).until(
        ec.alert_is_present()
    )
except TimeoutException as e:
    print("Error: Sorry was not able to find the element within required time")
finally:
    driver.quit()
```

#### frame_to_be_available_and_switch_to_it

### References:

- (For staleness_of) https://docs.seleniumhq.org/exceptions/stale_element_reference.jsp
- https://seleniumhq.github.io/selenium/docs/api/py/webdriver_support/selenium.webdriver.support.expected_conditions.html
