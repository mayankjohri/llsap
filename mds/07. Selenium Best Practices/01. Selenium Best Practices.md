
# Selenium Best Practices

- Mostly Use PageObjects pattern
- Be `generous` in 
    - return data 
    - reusing your model and functions
- Move most of the heavy lifting to base classes or Pageobject functions
- Keep code robust and portable 
- Prefered order for `select or`: `id` > `name` > `css` > `xpath` > `text` 
- use `Explicit wait` instead of `implicit wait`
- Use relative URLs and paths 
- Browser specific code should be avoided


- Keep testcases small
- randomize your dataset at runtime
- create dates for dataset at runtime 
