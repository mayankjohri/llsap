
# Finding Elements

One of the most important task in `selenium` automation is finding the correct way to identify the `elemnet` on the webpage and the reason why we need to identify them is so that we can perform operations on it. 

So lets get started. 

## Basics of Identifying the element on the webpage. 

Lets start our understanding of identifying the elements by finding the tools which are used to help us in this regards.

### `F12` is your friend

On all the browser (!!! Thank God for it, no in-browser fighting over it ;) !!!), pressing `F12` opens `Developers console/tools` (I will be calling it `"developers console"` from now on). 

On `Developers console`, we can find mutiple tabs with various names. as shown in the images below

#### Developers console of `Google Chrome`

![Chrome Developers Console](images/chrome_1.png)

At the top of `developers console`, we have multiple tables. Out of these, only `Element`, `Console` and `Sources` are of any interest to us. Lets discuss more about them.

##### Element tab

This is one of the most important `tab` which will be used to actually help in selecting the unique identifier for the element.

we will be providing the details in upcoming section "Discovering the unique identifier". 

##### Console Tab

`console` tab as the name suggest provide a console on which `js` expression can be executed and results viewed. We can also use it to validate the selectors.

##### Sources tab

`sources` tab can be used to view the content of the html page along with supporting files (css, js, etc).

#### Developers console of `Mozilla Firefox`

![Firefox Developers Console](images/firefox_1.png)

Tabs in firefox are similar to that of chorme, thus are not dicussing them. 
- `Inspector` in firefox is similar to that of "Elements" tab.

### Evaluating selectors using `console` tab

As previously stated, it can be used to evaluate `js` expressiong, we can use the following shortcuts  to find the elements 

| Shortcut  | Description                                                                  | Detailed                                                                                                | Example                                                   |
|-----------|------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| `$()`     | It selects the first element that matches the specified CSS selector.           | `document.querySelector()`                                                                              | `$('html')`                                               |
| `$$()`    | It selects an array of all the elements that match the specified CSS selector.  | `document.querySelectorAll()`                                                                           | `$$('div')`                                               |
| `$x()`    | It selects an array of elements that match the specified XPath.                 | `document.evaluate(path, document,  null,  XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;` | `$x('//*[@id="Evaluating-selectors-using-console-tab"]')` |

## Element selection strategies

Selenium provide multiple methods/techniques to identifing the `elements`. I have arranged the below table in order which can be used to identify the element on the webpage.

| Locator           | function                     | Description                                                        |
|-------------------|------------------------------|--------------------------------------------------------------------|
| id                | find_element_by_id           | Find elements which have ID attribute matching search value        |
| name              | find_element_by_name         | Find elements which have NAME attribute matching search value      |
| css selector      | find_element_by_css_selector | Find elements which match the CSS selector                         |
| link text         | find_element_by_link_text    | Find elements which have visible text matching search value        |
| partial link text | find_element_by_partial_link_text | Find element with visible text partially matching said value  |
| tag name          | find_element_by_tag_name          | Find element which have tag name matching search value        |
| class name        | find_element_by_class_name   | Find elements which have class name with the search value          |
| xpath             | find_element_by_xpath             | Find elements which have matching search XPath expression     |

Most of the `html` elements have `attributes` which can be used to identify them, such as `id`, `name`, `text`, `tag`, `class` and `css` properties.

Lets see how can we utilize them to identify them. 

### Using `id`

`html` elements usually has an attribute called `id`, and looks similar to shown in the image below. Normally, it is unique in a html page and thus can be used to identify the requested `element`

![images/find_element_by_id.png](images/find_element_by_id.png)

Once we get the unique `id` we can use the following `python` `command` to select the identifier. Full example in code repository as `find_by_id.py` file.

```python
driver.find_element_by_id("search")
```

> An `id` explicitly is **unique**. Only a single element in the document should have that `id`. And for that reason along its called an `id`, **a unique identifier**, and I hope web developers read it. :). 

> Its also a `W3C`  to have duplicate id's on the same page

#### Validating that on W3C Validator

- Navigate your browser to 
- Replace the following code in textarea
```html
<!doctype html>
<html>
    <head>
        <title>Test: Duplicate Id's</title>
    </head>
    <body>
        <input id="dup_id" type="text">
        <div id="dup_id">Find elements which have NAME attribute matching search value</div>
    </body>
</html>
```
- Click on "Check" button and view the result below

##### Result of W3C validation

![images/unique_id_w3c.png](images/unique_id_w3c.png)

### Using `name`

Unlike `id`, `name` attribute's are not unique on the page and more than one `element` can have same `name`. Thus using it, developers/automation enginner's need to be cautious.

#### Sample code with multiple `name` with same value.

```html
<form name="input" action="" method="get">
Username: <input type="text" name="user" />
<input type="submit" value="Submit" />
</form>

<div>Some text</div>
<form name="input" action="" method="get">
Username: <input type="text" name="user" />
<input type="submit" value="Submit" />
</form>
```

The `name` attribute can be identified as shown in below image.

![images/find_element_by_name.png](images/find_element_by_name.png)

Following `snippet code` can be used to identify the element using name. Detailed example is present as `find_by_name.py`.

```python
driver.find_element_by_name("user")
```

### Using `css`

Due to its flexibility and ease of use, it is the most used technique for identifying the elements. In order to use `css selectors` one need to have prior knowledge of basics of `css`.

As `css selectors` are so important that we will have a seperate chapter for it. 

Following snippet code can be used to identify the element using css. Detailed example is present as find_by_css.py

```python
driver.find_element_by_css_selector("div.file-wrap > table")
```

### find_element_by_link_text

Similar to `name`, they can be either `unique` or `not`, thus use them with **care**. Following snippet code can be used to identify the element using `find_element_by_link_text`. Detailed example is present as `find_by_link_text.py`

```python
driver.find_element_by_link_text("new")
```

### partial link text

Some time we have a large `text` in `link`, thus its not practical to use `find_element_by_link_text` in those cases `	find_element_by_partial_link_text` can be used.

```python
driver.find_element_by_partial_link_text("Reddit help")
```

### find_element_by_tag_name

Some `elements` can have `tag` attributes and it can be used to identify the element. Following snippet code can be used to identify the element using `find_element_by_tag_name`.

```python
driver.find_element_by_tag_name("test")
```

### find_element_by_class_name

It is usually used to find more than one elements. but it can also be used to find single element as shown in below snippet code. Detailed example is present as `find_by_class.py`

```python
driver.find_element_by_class_name("listingsignupbar__cta-desc")
```

### xpath

One of the most commonly used, but is also most prone to errors, if not done properly. We will not cover it in this version, but will add in later version.

## Tips

Following are few tips, which you can try while selecting the 

- Take the `webapp` developers to party and in return request them to add unique IDs, its the best solution. ;)
- Follow the Selection Algorithm. 
- Try to keep your locators (css, xpath) short and unique.
- When in doubt, look at parent
- Try mix and match to create your unique locator
- Get/Validate locators using browser inspector

### Take the `webapp` developers to party

The easiest way to get unique locator's are by getting the unique ID for the element. If you can communicate with the developers, request them to add unique id's for the elements. If they are not able to do it due to time constraint, please update the code yourself and send the updated code to them to include in `codebase`. This is the most robust method, nothing else comes even near it.   

### Follow the Selection Algorithm.

You can use following path/algorithm for selecting unique locators. 

```
ID -> NAME -> css_selector -> class_name -> xpath -> tag_name -> link_text -> partial_link_text
```

First start finding the unique locators using `ID` followed by `Name`, `CSS Selector`, `class_name`, `xpath`, `tag_name`, `link_text` and finally `partial_link_text`.  

### Try to keep your locators (css, xpath) short and unique.

Try to keep the locators short for css & xpath by using xpath & CSS shortcuts as longer locators most probable will not remain valid for long time, due to change in GUI over time. 

### When in doubt, look at parent

If you are not able to find any unique & stable locator using element's properties, only then start using its parent properties to create a unique locator. 
