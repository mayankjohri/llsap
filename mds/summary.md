# Summary

* [README](README.md)
* \[01. Getting Started\]
	* [00. Introduction](01. Getting Started/00. Introduction.md)
	* [01. Installing Selenium Webdriver](01. Getting Started/01. Installing Selenium Webdriver.md)
	* [02. Setting Development Environment](01. Getting Started/02. Setting Development Environment.md)
	* [03. Setting Test Environment](01. Getting Started/03. Setting Test Environment.md)
* \[02. Writing Tests\]
	* [01. Writing Test Cases](02. Writing Tests/01. Writing Test Cases.md)
	* [02. The unittest library](02. Writing Tests/02. The unittest library.md)
	* [03. The PyTest Library](02. Writing Tests/03. The PyTest Library.md)
* \[03. Finding Elements\]
	* [01. Finding Elements](03. Finding Elements/01. Finding Elements.md)
	* [02. CSS Selectors](03. Finding Elements/02. CSS Selectors.md)
* \[04. Element Interaction\]
	* [01. Element Properties](04. Element Interaction/01. Element Properties.md)
	* [02. Element Interaction](04. Element Interaction/02. Element Interaction.md)
	* [03. Select](04. Element Interaction/03. Select.md)
	* [04. Radio Button and Checkbox Buttons](04. Element Interaction/04. Radio Button and Checkbox Buttons.md)
	* [05. Progress bar](04. Element Interaction/05. Progress bar.md)
	* [06. Action Chains](04. Element Interaction/06. Action Chains.md)
	* [07. Cookies](04. Element Interaction/07. Cookies.md)
	* [08. Waits](04. Element Interaction/08. Waits.md)
* [05. Cross-browser Testing](05. Cross-browser Testing/Cross-browser Testing.md)
* [06. Page Objects Model](06. Page Objects Model/01. Page Object Model.md)
* [07. Selenium Best Practices](07. Selenium Best Practices/01. Selenium Best Practices.md)
