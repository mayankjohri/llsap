
## Installing Selenium Webdriver 

Python provide a third party package for the installation of selenium webdriver module. It can be installed using two methods. 

### Using PIP 

Use the following command to install the `Selenium API` for python, 

Install in `user profile`

```
pip install selenium --user
```

- Install at `machine level`

```
pip install selenium
```

### Error Message when drivers are not in path

```log
Traceback (most recent call last):
  File "/home/mayank/.local/lib64/python3.6/site-packages/selenium/webdriver/common/service.py", line 76, in start
    stdin=PIPE)
  File "/usr/lib64/python3.6/subprocess.py", line 707, in __init__
    restore_signals, start_new_session)
  File "/usr/lib64/python3.6/subprocess.py", line 1326, in _execute_child
    raise child_exception_type(errno_num, err_msg)
FileNotFoundError: [Errno 2] No such file or directory: 'geckodriver'

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "basic_firefox.py", line 3, in <module>
    browser = webdriver.Firefox()
  File "/home/mayank/.local/lib64/python3.6/site-packages/selenium/webdriver/firefox/webdriver.py", line 152, in __init__
    self.service.start()
  File "/home/mayank/.local/lib64/python3.6/site-packages/selenium/webdriver/common/service.py", line 83, in start
    os.path.basename(self.path), self.start_error_message)
selenium.common.exceptions.WebDriverException: Message: 'geckodriver' executable needs to be in PATH. 
```

