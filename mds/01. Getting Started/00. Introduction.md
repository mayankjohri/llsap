
## Introduction

I have created this book with one purpose in mind, that this book can eventually be used as reference for the tasks at hand. For this reason it contains less theory and more code.

Also note, *that this book is and will always be work under progress, thus you will see lots of `TODO` in it.* 

**I will try to keep this book updated. the version of the book can be checked at the bottom of the cover page. If your's is older, you can request for the latest version from your respective market (Amazon kindle, Google Play etc).**

### Appeal to readers

Please provide your feedback to me either though website or email them to me at `funmayank@yahoo.co.in` just add **suggestion/clarification/issues: [book name]** in subject so I can find the context and segregate the emails without issue
