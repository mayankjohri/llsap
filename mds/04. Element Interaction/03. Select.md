
## Select

### Selecting value using `select_by_visible_text`

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.select import Select


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

ele = driver.find_element_by_id("cleanest")
select = Select(ele)
select.select_by_visible_text("Bhopal")
```

### Selecting value using `deselect_by_visible_text`

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.select import Select


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

ele = driver.find_element_by_id("cleanest")
select = Select(ele)
select.select_by_value("Navi Mumbai")
select.select_by_value("Tirupati")
select.select_by_value("Bhopal")
select.select_by_value("Indore")
select.deselect_by_visible_text("Tirupati")
```

### Selecting value using `select_by_value`

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.select import Select


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

ele = driver.find_element_by_id("cleanest")
select = Select(ele)
select.select_by_value("Bhopal")
```

### Selecting value using `deselect_by_value`

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.select import Select


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

ele = driver.find_element_by_id("cleanest")
select = Select(ele)
select.select_by_value("Navi Mumbai")
select.select_by_value("Tirupati")
select.select_by_value("Bhopal")
select.select_by_value("Indore")
select.deselect_by_value("Tirupati")

```

### Selecting value using `select_by_index`

```python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.select import Select


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

ele = driver.find_element_by_id("cleanest")
select = Select(ele)
select.select_by_index(4)
```

### `deselect_by_index(indx)`

```python
#!/usr/bin/env python
# c#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.select import Select


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

ele = driver.find_element_by_id("cleanest")
select = Select(ele)
select.select_by_value("Navi Mumbai")
select.select_by_value("Tirupati")
select.select_by_value("Bhopal")
select.select_by_value("Indore")
select.deselect_by_index(6)
```

### `deselect_all()`

```Python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.select import Select


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

ele = driver.find_element_by_id("cleanest")
select = Select(ele)
select.select_by_value("Navi Mumbai")
select.select_by_value("Tirupati")
select.select_by_value("Bhopal")
select.select_by_value("Indore")
select.deselect_all()
```


### `all_selected_options()`

```Python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.select import Select


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

ele = driver.find_element_by_id("cleanest")
select = Select(ele)
select.select_by_value("Navi Mumbai")
select.select_by_value("Tirupati")
select.select_by_value("Bhopal")
select.select_by_value("Indore")
print(len(select.all_selected_options))
print(select.all_selected_options)
```


**Output**
```
4
[<selenium.webdriver.remote.webelement.WebElement (session="c40a879ef10514fd7cf9aadcd0660a00", element="0.046842233448646464-2")>, <selenium.webdriver.remote.webelement.WebElement (session="c40a879ef10514fd7cf9aadcd0660a00", element="0.046842233448646464-3")>, <selenium.webdriver.remote.webelement.WebElement (session="c40a879ef10514fd7cf9aadcd0660a00", element="0.046842233448646464-4")>, <selenium.webdriver.remote.webelement.WebElement (session="c40a879ef10514fd7cf9aadcd0660a00", element="0.046842233448646464-5")>]
```

### `first_selected_option()`

```Python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.select import Select


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

ele = driver.find_element_by_id("cleanest")
select = Select(ele)
select.select_by_value("Navi Mumbai")
select.select_by_value("Tirupati")
select.select_by_value("Bhopal")
select.select_by_value("Indore")
print(len(select.all_selected_options))
print(select.first_selected_option.text)
```


**output**
```
4
Navi Mumbai
```

### options()

```Python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.select import Select


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

ele = driver.find_element_by_id("cleanest")
select = Select(ele)
select.select_by_value("Navi Mumbai")
select.select_by_value("Tirupati")
select.select_by_value("Bhopal")
select.select_by_value("Indore")
for opt in select.options:
    print(opt.text)
```

**Output**
```
Visakhapatnam
Surat
Mysore
Tiruchirapalli
New Delhi Municipal Council
Navi Mumbai
Tirupati
Bhopal
Indore
```

### `is_multiple()`

```Python
#!/usr/bin/env python
# coding=utf-8
"""."""
from selenium import webdriver
from selenium.webdriver.support.select import Select


options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/google-chrome"
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://127.0.0.1:5000/form_bootstrap_dropdown")

ele = driver.find_element_by_id("cleanest")
select = Select(ele)
print(select.is_multiple)
```

**Output**
```
True
```
